var app = angular.module('imdApp', []);

app.controller('myController', function($scope){
	console.log('hi there!', $scope);
  
  $scope.myName = 'Angular';
  
  $scope.products = [{
    name:'Cola',
    qty: 0
  },{
    name:'Fanta',
    qty: 0
  },{
    name:'Jupiler',
    qty: 0
  },{
    name:'Capri-sun',
    qty: 0
  }];
  
  $scope.calculate = function calculate() {
    $scope.result = $scope.mod1*$scope.mod2;
  };
  
  $scope.$watch('mod1', function(){
    $scope.calculate();
  });
  
  $scope.$watch('mod2', function(){
    $scope.calculate();
  });
});

app.directive('amountControl', function(){
  return {
    template: '<button ng-click="qty = qty + 1">+</button><button ng-click="qty = qty - 1">-</button>',
    restrict: 'AEC',
    scope: {
      qty: '=qty'
    },
    link: function ($scope, element, attributes) {
      
    }
  };
});