$(document).ready(function() {
"use strict";
    /*global $, jQuery, console*/
		var w = document.getElementById('graden');	
        var ico = document.getElementById('icoon');
        var min = document.getElementById('mintemp');
        var max = document.getElementById('maxtemp');
        var day1 = document.getElementById('day1');
        var day2 = document.getElementById('day2');
        var day3 = document.getElementById('day3');
	
		function getLocation()
		  {
			  if (navigator.geolocation)
			  {
			    navigator.geolocation.getCurrentPosition(showPosition);
			    navigator.geolocation.getCurrentPosition(getweather);
                  
			  }
			  else
			  {
			  	alert("Geolocation is not supported by this browser.");
			  }
		  }
        
		function showPosition(position)
		  {
		  console.log("Latitude: " + position.coords.latitude + 
		  "<br>Longitude: " + position.coords.longitude); 
		  }
		function getweather(position)
		
{			$.ajax({
			    url: "https://api.forecast.io/forecast/ffd23729891a3f4e3b3856dce161c767/"+ position.coords.latitude + ","+ position.coords.longitude,
			 
			    // the name of the callback parameter, as specified by the YQL service
			    jsonp: "callback",
			 
			    // tell jQuery we're expecting JSONP
			    dataType: "jsonp",
			 
			    // tell YQL what we want and that we want JSON
			    data: {
			        q: "select title,abstract,url from search.news where query=\"cat\"",
			        format: "json"
			    },
 
    // work with the response
    success: function( response ) {
        console.log( response ); // server response
        w.innerHTML = Math.round((response.currently.temperature - 32)/1.8) + " <strong>&degC</strong>";
        min.innerHTML = "MIN: " + Math.round((response.daily.data[0].temperatureMin - 32)/1.8) + " <strong>&degC</strong>";
        max.innerHTML = "MAX: " + Math.round((response.daily.data[0].temperatureMax - 32)/1.8) + " <strong>&degC</strong>";
        
        day1.innerHTML =  "Tomorrow: "+ Math.round((response.daily.data[1].temperatureMax - 32)/1.8) + " <strong>&degC</strong>";
        day2.innerHTML =  "Day after tomorrow: "+ Math.round((response.daily.data[2].temperatureMax - 32)/1.8) + " <strong>&degC</strong>";
        day3.innerHTML =  "Day 3: " + Math.round((response.daily.data[3].temperatureMax - 32)/1.8) + " <strong>&degC</strong>";
		var i = response.currently.icon;
					switch(i){
						case "clear-day":
							ico.src="http://www.iconsdb.com/icons/preview/black/sun-xxl.png";
							break;

						case "clear-night":
							ico.src="http://www.iconsdb.com/icons/preview/black/moon-xxl.png";
							break;

						case "partly-cloudy-day":
							ico.src="http://www.iconsdb.com/icons/preview/black/partly-cloudy-day-xxl.png";
							break;
						case "partly-cloudy-night":
							ico.src="http://www.iconsdb.com/icons/preview/black/partly-cloudy-night-xxl.png";
							break;
						case "wind":
							ico.src="http://www.iconsdb.com/icons/preview/black/wind-turbine-xxl.png";
							break;
						default:
							ico.src="http://www.iconsdb.com/icons/preview/black/clouds-xxl.png";
					}

					var temperatuur = Math.round((response.currently.temperature - 32)/1.8)
					

					switch(true){
						   	case temperatuur < -20:
						      $('.container').css("background-color", "#C5EFF7");
						      break;
						  	 case temperatuur < -15:
						      $('.container').css("background-color", "#81CFE0");
						      break;
						   	case temperatuur  < -10:
						      $('.container').css("background-color", "#59ABE3");
						      break;
						  	case temperatuur  < -5:
						      $('.container').css("background-color", "#19B5FE");
						      break;
						    case temperatuur  < 0:
						      $('.container').css("background-color", "#ffffff");
						      break;
						    case temperatuur  < 5:
						      $('.container').css("background-color", "#FDE3A7");
						      break;
						    case temperatuur  < 10:
						      $('.container').css("background-color", "#F9BF3B");
						      break;
						    case temperatuur  < 15:
						      $('.container').css("background-color", "#F5D76E");
						      break;
						    case temperatuur  < 20:
						      $('.container').css("background-color", "#F5AB35");
						      break;
						    case temperatuur  < 25:
						      $('.container').css("background-color", "#D35400");
						      break;
						    case temperatuur  < 30:
						      $('.container').css("background-color", "#EF4836");
						      break;
						    case temperatuur  < 35:
						      $('.container').css("background-color", "#D64541");
						      break;




						}

				
    }
});
		}
		 getLocation();
		 
	});